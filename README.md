# VoTonic

This project plans to reverse engineer and document the protocol used by the inverters, solar charge controllers, tank sensors, displays etc. produced by the German company [Votronic](https://votronic.de/).
These products can be found in recreational and expedition vehicles, boats and so on.

This repository is a fork of the original project by [Scy](https://codeberg.org/scy).

Note:
There is also the [S-BC Bluetooth Connector](https://www.votronic.de/index.php/en/products/measuring-display-systems/energy-monitor-app) available from Votronic.
If all you want to do is interface your setup with a smartphone, this might be your way to go.
When I started working on this project, this component didn’t exist yet.
And now, I kind of want to finish it anyway.

## Status

Work in progress.

I’ve figured out the physical/electrical setup (RS485 with RJ45 connectors) and am currently trying to make sense of the serial protocol that’s spoken on the wire.

## Hardware I’m Using

### Bus System VBS2 Master Box

This is the heart of the system, with one wire screw connection bar each at the top and bottom, interfacing to non-bus components.

It also has three RJ45 connections labeled “Bus”, which are (in my setup) connected to

* the Display / control unit
* the Equipment Adapter and
* the Solar/Battery Charger

### [Battery Charger Triple VBCS 45/30/350](https://www.votronic.de/index.php/en/products2/battery-charger-series-vbcs-triple/standard-version/vbcs-45-30-350)

Solar Charge Controller and Battery Charger.
Connects to both the ignition and RV batteries.
Also has a bus connection.

I don’t mess with this thing, it has big wires.

### [MobilPOWER Inverter SMI 1700 Sinus ST-NVS](https://www.votronic.de/index.php/en/products2/sine-inverters/standard-version/smi-1700-st-nvs)

Does the 230V shizzle.
Isn’t directly connected to the bus, but via the EQ-BCI adapter described below.

### Equipment Adapter EQ-BCI

This is a matchbox-sized device that has two RJ45 jacks labeled “Bus” and one smaller jack (RJ12? have to check) labeled “B2B/Charger/Inverter”.
It apparently translates between whatever protocol my inverter is using and the bus protocol analyzed in this project.

Since it has two bus connections, of which only one was used when I got my camper van, it was the perfect candidate to start reverse engineering and tapping into the bus.
Looking at the chips inside the Equipment Adapter made me realize that the bus is using RS485, and looking at the datasheets of the ICs allowed me to reverse engineer the RJ45 pin mapping.

#### Board Contents

The top of the board contains:

* `6N137` optocoupler
* 25V 100µF capacitor
* ZTT 8.00MT 8MHz ceramic resonator
* diode labeled “Z 15”

The bottom of the board contains:

* ATMEL ATMEGA8L 8AU 1710D
* 7LB176 68M AKY5 [RS-485 transciever](http://www.ti.com/lit/ds/slls067h/slls067h.pdf)
  * pin 1 is at the bottom left (holding it so that the label on the IC is upright)
* 2× 690Y 2951 CMC [voltage regulators](http://ww1.microchip.com/downloads/en/DeviceDoc/mic2950.pdf), one for each bus port, apparently for power supply
* TCLT1002 V 727 68 [optocoupler](https://www.vishay.com/docs/83515/tclt1000.pdf)

## Values of Interest

On the control unit, I can see the following values:

* RV battery
  * remaining capacity percentage
  * voltage
  * current (negative if discharging, positive if charging)
  * remaining capacity in Ah
* starter battery
  * voltage
* fresh water tank level percentage
* grey water tank level percentage
* indoor temperature
* outdoor temperature
* clock
* solar current in A
* status LEDs
  * 230V external power
  * charging battery (from 230V, I presume)
  * inverter
    * providing 230V
    * overload
    * manual
    * automatic

However, the indoor temperature sensor is connected directly to the display, so it’s quite possible that its value may not be available via the bus.

## Physical Connection

Most of the products I’m using connect through an 8-wire RJ45 jack.
You can use cheap (flat) phone cable or twisted-pair (Ethernet) cables to connect to them.

When using a cable with T568B colors, pin 1 uses the white/orange wire; T568A use white/green on pin 1.

The pinout looks like this:

* **1:** not sure, seems not connected or reserved
* **2 & 3:** Vin (power supply)
* **4:** RS485 A wire
* **5:** RS485 B wire
* **6, 7 & 8:** ground

## RS485 / UART properties

The wire protocol has a transfer rate of 19,200 bits per second, 8 bit, with even parity and 1 stop bit.

Linux users can use the following command to set up their RS485 interface:

```sh
stty -F /dev/ttyWHATEVER 19200 cs8 -cstopb parenb -parodd raw
```

I’m using [this RS485 USB adapter](https://www.amazon.de/USB-RS485-Adapter-mit-Gehäuse/dp/B00I9H5J02).

If you want to look at the raw data using `hexdump`, I’d use something like

```sh
stdbuf -i0 -o0 -e0 hexdump -Cv /dev/ttyWHATEVER
```

## Protocol

Most of this is speculation.
Don’t rely on it yet.
All byte values are written in hexadecimal.

There seems to be no collision avoidance on the bus.
Devices seem to talk whenever they want, even while another transmission is already running.
This leads to something like 5 to 10 % of packets looking wrong:
They’re longer than expected because they’re two packets mushed together, where one starts somewhere in the middle of the other.

![diagram](https://codeberg.org/reijkoekl/votonic/raw/branch/main/message-format.png)

[Diagram source](http://interactive.blockdiag.com/packetdiag/?compression=deflate&src=eJw1zE0KwjAQhuF9T_FdQGj9N-BKDyAIbmVMhjaYJiWdIkW8u07A5fsM3wxknyzOU4t3BcCm8PJOOhxx0I7J8f0vTV1XirXBJTP1j8CazS_LG8g8FFkanHkUH0l8iiorg2uasi3ntQ7mkMghcGylU9woZupZOGtvFzuDG4WpTPYGp47tc5z66vMFrng0CA)

Every packet on the bus I’ve seen seems to be 9 bytes long and consists of the following parts:

* A preamble: 0xAA
* Package type
  * 0x22: request
  * 0x62: response
* Destination (s. below for known devices)
* Source
* Command
  * 0x03: read value
  * 0x09: unknown (possible write command, because seen with values other than 0 in value field)
* Parameter ID (s. below for known values)
* Value: Two bytes, Little-endian, can apparently contain different types or two 1 byte values.
* Checksum, calculated by starting with `0x55` and then xor'ing each byte in the packet (including the `aa`). (Thanks [Manawyrm](https://twitter.com/Manawyrm/status/1215915479228997632) for finding it out!)

### Devices

| Address  | Device                 | Parameters                   |
|----------|------------------------|------------------------------|
| 0x0C     | RV battery management  | 0x02: Current consumption (deciamps) <br/> 0x03: Battery voltage (centivolts) <br/> 0x05: Charge (Ah) <br/> 0x06: Low byte: Charge (%) High byte: unknown |
| 0x10     | Solar converter        | 0x02: Current input (deciamps)    |
| 0x14     | Fresh water sensor     | 0x02: Level (%)    |
| 0x18     | Gray water sensor      | 0x02: Level (%)    |
| 0x30     | Unknown                | 0x02: Unknown (reads 0x4B in my vehicle) |
| 0x30     | Unknown      | 0x02: Unknown (reads 0x4B in my vehicle)    |
| 0x34     | Unknown      | 0x00: Unknown (reads 0xE1 in my vehicle)    |
| 0x38     | Unknown      | 0x00: Unknown (reads 0x00 in my vehicle)    |
| 0x44     | Starter battery        | 0x03: Battery voltage (centivolts)  |
| 0x74     | Unknown      | 0x02: Unknown (reads 0x00 in my vehicle)    |
| 0xAC     | Unknown      | 0x02: Unknown (reads 0x30 in my vehicle)    |
| 0xB0     | Unknown      | 0x04: Unknown (reads 0x0404 in my vehicle, some LEDs?) <br/> 0x1B: unknown (device 0xAC sets this value to 0x20 using command 0x09 in my vehicle)  |
| 0xBB     | Unknown      | 0x03: Unknown (reads 0x00 in my vehicle)    |
| 0xF4     | Bus Master/ Display      | This device initiates all the reads from the other devices |


### There’s a typo in the project name!

No there’s not.
For trademark reasons, this project is named after [vodka tonic](https://en.wikipedia.org/wiki/Vodka_tonic).
Because why not!
